###
Building Docker Image Dockerfile
###

For lumen laravel application Docerfile is present in this repo.

clone this repo and go to repo directory

For creating docker image use this command

docker build -t php:lumen -f Dockerfile .

after running this command docker image will be build and ready to use it for docker container

###
Running Docker Container
###

We have docker image now so we can run our application in docker container

we run these commands inside our code directory

docker create network lumen

docker run -d --name lumen --network lumen -p 8000:8000 -v $(pwd):/var/www/html/ php:lumen

###
Local code change reflect in running container
###

As above docker running command we have mounted volume with $(pwd) directory where our code is present so when we change something inside that directory it will reflect in running container.

###
Fresh code running inside container
###

For this we can use latest code in our directory which is mounted or we can use git pull inside our directory

###
Mysql 5.7 for local
###

we need to install mysql 5.7 in our local env.

we can easily find ways how to install it i am skiping it.

###
Database Mysql connection with application
###

We use .env file with environment variables for connection with db and application.

.env also included in repo.

###
Xdebug
###
xdebug installed in docker image and its configuration file also included in image

