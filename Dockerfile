# taking base image as i have selected official php docker image
# we can also use some other images but best practice is to use official images
FROM php:7.2.14-apache

#install required packages

RUN apt update -y && apt-get install -y telnet net-tools unzip git zip nano cron curl wget &&  curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

#install required php lumen extensions
RUN docker-php-ext-install mbstring tokenizer mysqli pdo_mysql

#xdebug install
RUN pecl install xdebug

COPY xdebug.ini /etc/php/7.0/mods-available/xdebug.ini
#RUN apache2
COPY apache2.conf /etc/apache2/sites-available/000-default.conf
RUN a2enmod rewrite && \
    service apache2 start && \
    service apache2 reload && \
    service apache2 restart
#start script
COPY start.sh /start.sh
RUN chmod 500 /start.sh
#working directory
WORKDIR /var/www/html/
#port expose
EXPOSE 8000

#starting point of container
CMD ["/start.sh"]




